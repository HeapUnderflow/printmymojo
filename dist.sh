#!/usr/bin/env sh

set -eu

echo "[Pre Clean]"
rm -Rf dist || true
echo "[Comp]"
yarn shadow-cljs release app

echo "[Copying Files]"
mkdir dist
mkdir dist/js
mkdir dist/css

cp resources/public/index.html dist/
cp -R resources/public/css/* dist/css/
cp -R resources/public/js/* dist/js/

echo "[Done]"
