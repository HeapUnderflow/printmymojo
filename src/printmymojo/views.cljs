(ns printmymojo.views
  (:require
   [re-frame.core :as rf]
   [printmymojo.subs :as subs]
   [printmymojo.events :as events]))

(def class-num-input "bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500")
(def class-num-label "block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4")
(def class-tex-input "m-4 box-border appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500 h-full")

(defn size-input []
  (let [w @(rf/subscribe [::subs/width])
        h @(rf/subscribe [::subs/height])
        getv (fn [e] (-> e .-target .-value))
        emit-w (fn [e] (rf/dispatch [::events/change-width (getv e)]))
        emit-h (fn [e] (rf/dispatch [::events/change-height (getv e)]))]
    [:div.w-full.max-w-sm.mt-4
     [:div.flex.items-center
      [:span {:class class-num-label} "Width: "]
      [:span#input-w.pr-4 [:input {:class class-num-input
                                   :type "number"
                                   :value w
                                   :on-change emit-w
                                   :min 1}]]
      [:span {:class class-num-label} "Height: "]
      [:span#input-h [:input {:class class-num-input
                              :type "number"
                              :value h
                              :on-change emit-h
                              :min 1}]]]]))

(defn text-panel []
  (let [text      @(rf/subscribe [::subs/text])
        converted @(rf/subscribe [::subs/converted])
        emit (fn [e] (rf/dispatch [::events/change-text (-> e .-target .-value)]))]
     [:div.mt-4.flex
      [:div
       [:h2.text-xl.font-semibold "Input:"]
       [:textarea#input-t {:class class-tex-input
                           :type "textarea"
                           :lines 100
                           :cols 100
                           :value text
                           :on-change emit}]]
      [:div.m-auto
       [:h2.text-xl.font-semibold "Result:"]
       [:pre.shadow.rounded.p-2 converted]]]))

(defn main-panel []
  [:div.m-4
   [:h1.text-3xl.font-semibold "Print my Mojo"]
   (size-input)
   (text-panel)])
