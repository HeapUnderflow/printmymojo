(ns printmymojo.logic
  (:require [clojure.string :as str]))

(defn calculate-text
  [t w h]
  (let [fill (repeat w " ")
        empty (str/join "" fill)]
    (str/join "\n"
              (flatten
               (map (fn [r]
                      (if (= "" r)
                        empty
                        (map
                         (fn [v] (str/join "" v))
                         (partition w w fill r))))
                    (take h (str/split t #"\n")))))))
