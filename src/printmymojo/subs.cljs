(ns printmymojo.subs
  (:require
   [re-frame.core :as re-frame]
   [printmymojo.logic :as logic]))

(re-frame/reg-sub
 ::text
 (fn [db _]
   (:text db)))

(re-frame/reg-sub
 ::converted

 :<- [::text]
 :<- [::width]
 :<- [::height]

 (fn [[t w h] _]
   (logic/calculate-text t w h)))

(re-frame/reg-sub
 ::width
 (fn [db _]
   (:width db)))

(re-frame/reg-sub
 ::height
 (fn [db _]
   (:height db)))
