(ns printmymojo.events
  (:require
   [re-frame.core :as re-frame]
   [printmymojo.db :as db]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   ))

(re-frame/reg-event-db
 ::initialize-db
 (fn-traced [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::change-width
 (fn [db [_ new-width]]
   (let [n (parse-long new-width)]
     (assoc db :width (if (< n 1) 1 n)))))


(re-frame/reg-event-db
 ::change-height
 (fn [db [_ new-height]]
   (let [n (parse-long new-height)]
     (assoc db :height (if (< n 1) 1 n)))))

(re-frame/reg-event-db
 ::change-text
 (fn [db [_ new-text]]
   (assoc db :text new-text)))
